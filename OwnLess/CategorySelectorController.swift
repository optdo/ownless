//
//  CategorySelectorViewController
//  OwnLess
//
//  Created by Bastiaan on 04/03/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class CategorySelectorViewController: UITableViewController, CategoryDetailDelegate {
    
    var categories = [NSManagedObject]()
    var selectedItem:String? = nil
    var selectedItemId:String? = nil
    var selectedItemIndex:Int? = nil
    
    let undoer = UndoManager()
    
    override var undoManager : UndoManager {
        get {
            return self.undoer
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count + 1
    }
    
    @IBAction func add(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Category", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CategoryDetailView") as! CategoryDetailViewController
        
        let navigationController = UINavigationController(rootViewController: viewController)
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        self.tableView.reloadData()
    }
    
    func loadItems() {
        categories = loadCategories()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        
        // Configure the cell...
        if (indexPath as NSIndexPath).row == 0 {
            cell.textLabel!.text = "Add new category"
            cell.textLabel!.textColor = UIColor.gray
            cell.accessoryType = .disclosureIndicator
        }
        else {
            let item = categories[(indexPath as NSIndexPath).row - 1]
            cell.textLabel!.text = item.value(forKey: "name") as! String?
            
            if item.value(forKey: "id") as! String? == selectedItemId {
                cell.accessoryType = .checkmark
                selectedItemIndex = (indexPath as NSIndexPath).row - 1
                //               selectedItemId = item.value(forKey: "id") as! String?
                selectedItem = item.value(forKey: "name") as! String?
            } else {
                cell.accessoryType = .none
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteClosure = { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
            let itemToDelete = self.categories[indexPath.row]
            
            self.deleteItem(itemToDelete)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        /*
         let editClosure = { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
         // open project detail view
         let storyboard = UIStoryboard(name: "Project", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "ProjectDetailView") as! ProjectDetailViewController
         let navigationController = UINavigationController(rootViewController: vc)
         vc.project = self.items[indexPath.row]
         vc.title = "Project"
         vc.delegate = self
         
         self.present(navigationController, animated: true, completion: nil)
         }*/
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: deleteClosure)
        //        let editAction = UITableViewRowAction(style: .normal, title: "Edit", handler: editClosure)
        //        let summaryAction = UITableViewRowAction(style: .normal, title: "Summary", handler: summaryClosure)
        
        //        summaryAction.backgroundColor = UIColor.darkGray
        
        return [deleteAction]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        //Other row is selected - need to deselect it
        if var index = selectedItemIndex {
            index += 1
            let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
            cell?.accessoryType = .none
        }
        
        if (indexPath as NSIndexPath).row == 0 {
            // open new project view
            let storyboard = UIStoryboard(name: "Category", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "CategoryDetailView") as! CategoryDetailViewController
            
            let navigationController = UINavigationController(rootViewController: viewController)
            
            viewController.delegate = self
            
            self.present(navigationController, animated: true, completion: nil)
        }
        else {
            //update the checkmark for the current row
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = .checkmark
            selectedItemIndex = (indexPath as NSIndexPath).row - 1
            let item = categories[selectedItemIndex!]
            selectedItemId = item.value(forKey: "id") as! String?
            //        println(selectedItemId)
            selectedItem = item.value(forKey: "name") as! String?
            //       println(selectedItem)
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func deleteItem(_ itemToDelete:NSManagedObject)
    {
        undoManager.registerUndo(withTarget: self, selector:#selector(CategoriesViewController.deleteItem(_:)), object:itemToDelete)
        undoManager.setActionName("delete category")
        
        // Delete it from the managedObjectContext
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        if self.undoer.isUndoing {
            itemToDelete.setValue(false, forKey: "trash")
        }
        else
        {
            itemToDelete.setValue(true, forKey: "trash")
        }
        
        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }
        
        // Refresh the table view to indicate that it's deleted
        self.loadItems()
        
        if self.undoer.isUndoing || self.undoer.isRedoing {
            self.tableView.reloadData()
        }
    }
    
    func deleteCategory(_ controller: CategoryDetailViewController) {
        deleteItem(controller.category)
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewController(animated: true)
    }
    
    func selectCategory(_ controller: CategoryDetailViewController) {
        selectedItemId = controller.categoryId
        selectedItemIndex = nil
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewController(animated: true)
    }
}
