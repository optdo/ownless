//
//  AccountViewController.swift
//  OwnLess
//
//  Created by Bastiaan on 04/03/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class HomeViewController: UITableViewController {
    
    
    @IBOutlet var costPerMonthLabel: UILabel!
    @IBOutlet var totalPurchaseValueLabel: UILabel!
    @IBOutlet var depriciatedLabel: UILabel!
    @IBOutlet var itemsLabel: UILabel!
    
    var items = [NSManagedObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    @IBAction func addItem(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Item", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ItemDetailView") as! ItemDetailViewController
        
        let navigationController = UINavigationController(rootViewController: viewController)
        
        viewController.title = "New Item"
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        
        var totalitems : Int = 0
        var totalpurchasevalue : Double = 0
        var totalcosts : Double = 0
        var totaldepreciation : Double = 0
 //       let today = Date()
        //    var totaltime : TimeInterval = 0
        
        for resultItem : AnyObject in items {
            //        var time : TimeInterval = 0
            let purchasevalue = resultItem.value(forKey: "purchasevalue") as! Double
            let costs = resultItem.value(forKey: "monthlycosts") as! Double
            let deprication = resultItem.value(forKey: "depreciation") as! Double
            //            let residualvalue = resultItem.value(forKey: "residualvalue") as! Double
/*
            var acquired : Date! = nil
            acquired = resultItem.value(forKey: "acquired") as! Date?
            let retirement = resultItem.value(forKey: "retirement") as! Date?
            let replace = resultItem.value(forKey: "replace") as! Bool

            if (replace && retirement != nil && retirement! > today) {
                let dateComponents =  Calendar.current.dateComponents([.weekOfYear, .month], from: acquired!, to: today)
                let months = dateComponents.month
                
                totaldepreciation += Double(months!) * costs
            } else if (replace && retirement != nil)
            {
                totaldepreciation += (purchasevalue - residualvalue)
            }*/
            totaldepreciation += deprication
            
            totalitems += 1
            totalcosts += costs
            totalpurchasevalue += purchasevalue
        }
        

        
        itemsLabel.text = String(format: "%d", totalitems)
        totalPurchaseValueLabel.text = NSString(format: "%.0f", totalpurchasevalue) as String
        costPerMonthLabel.text = String(format: "%.0f", totalcosts)
        depriciatedLabel.text = String(format: "%.0f", totaldepreciation)
        
        var toolBarItems = [UIBarButtonItem]()

        let systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBarItems.append(systemButton1)
        
        let systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.compose, target: self, action: #selector(HomeViewController.addItem(_:)))
        toolBarItems.append(systemButton2)
        
        self.navigationController?.isToolbarHidden = false
        self.setToolbarItems(toolBarItems, animated: true)
    }
    
    func loadItems() {
        items = getItems(categoryId: nil, accountID: nil, manufacturerId: nil, replace: false, sort: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowReplace" {
            let itemsViewController = segue.destination as! ItemsViewController
            itemsViewController.replace = true
        }
    }

    
 
}
