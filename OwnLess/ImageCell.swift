//
//  ImageCell.swift
//  OwnLess
//
//  Created by Bastiaan on 25/06/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit

class ImageCell : UICollectionViewCell {
 
    @IBOutlet var imageView: UIImageView!
    
    
}
