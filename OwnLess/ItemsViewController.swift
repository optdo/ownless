//
//  ItemViewController.swift
//  OwnLess
//
//  Created by Bastiaan on 04/03/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class ItemsViewController: UITableViewController, ItemDetailDelegate {

    
    @IBOutlet var cellImage: UIImageView!
    @IBOutlet var cellTitle: UILabel!
    @IBOutlet var cellSubtitle: UILabel!
    @IBOutlet var cellSubtitle2: UILabel!
    @IBOutlet var cellSubtitle3: UILabel!
    
    var accountid: String?
    var categoryid: String?
    var manufacturerid: String?
    var replace: Bool? = false
    var sort: String? = "bydate"
    var totalCosts: Double = 0.0
    var previousId: String = ""
    var groupname : String = ""

    //var totalItems : Double = 0
    
    var items = [NSManagedObject]()

    fileprivate var statusButton: UIButton = UIButton()

    let undoer = UndoManager()
    
    override var undoManager : UndoManager {
        get {
            return self.undoer
        }
    }
    
    @objc func changeSort(_ sender: UIBarButtonItem)
    {
        if(sort == "bydate") {
            sort = "bycosts"
//            sender.title = "Sorted by Monthly Costs"
        }
        else if(sort == "bycosts") {
            sort = "bycategory"
//            sender.title = "Sorted by Category"
        }
        else if(sort == "bycategory") {
            sort = "bymanufacturer"
//            sender.title = "Sorted by Manufacturer"
        }
        else if(sort == "bymanufacturer") {
            sort = "byaccount"
 //           sender.title = "Sorted by Account"
        }
        else if(sort == "byaccount") {
            sort = "bydepreciation"
 //           sender.title = "Sorted by Depreciation"
        }
        else {
            sort = "bydate"
 //           sender.title = "Sorted by Replacement Date"
        }
        
        loadItems()
        self.tableView.reloadData()
        updateStatusButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        var toolBarItems = [UIBarButtonItem]()
        
        //let systemButton1 = UIBarButtonItem(title: "Completed", style: .plain, target: self, action: #selector(ItemsViewController.changeSort(_:)))
        
        //let font = UIFont.systemFont(ofSize: 11.0)
        //systemButton1.setTitleTextAttributes([NSAttributedStringKey.font: font], for: UIControlState())
        
        //systemButton1.tintColor = UIColor.blue
        //if(sort == "bycosts") {
        //    systemButton1.title = "Sorted by Monthly Cost"
        //}
        //else {
            //            systemButton1.tintColor = UIColor.grayColor()
        //    systemButton1.title = "Sorted by Replacement Date"
        //}
        //toolBarItems.append(systemButton1)
        
        let systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.organize, target: self, action: #selector(ItemsViewController.changeSort(_:)))
        toolBarItems.append(systemButton1)

        let systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        toolBarItems.append(systemButton2)
        
        let systemButton3 = UIBarButtonItem(title: "Sort", style: .plain, target: self, action: #selector(ItemsViewController.changeSort(_:)))
        
        statusButton.titleLabel?.numberOfLines = 3
        statusButton.setTitleColor(UIColor.gray, for: UIControlState())
        statusButton.titleLabel!.font =  UIFont.systemFont(ofSize: 11.0)
        statusButton.titleLabel!.textAlignment = .center
        statusButton.sizeToFit()
        systemButton3.customView = statusButton
        
        toolBarItems.append(systemButton3)

        toolBarItems.append(systemButton2)
        
        let systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.compose, target: self, action: #selector(ItemsViewController.add(_:)))
        toolBarItems.append(systemButton4)
        
        self.navigationController?.isToolbarHidden = false
        self.setToolbarItems(toolBarItems as [UIBarButtonItem], animated: true)

        updateStatusButton()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    @IBAction func add(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Item", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ItemDetailView") as! ItemDetailViewController
        
        let navigationController = UINavigationController(rootViewController: viewController)
        
        viewController.accountid = accountid
        viewController.categoryid = categoryid
        viewController.manufacturerid = manufacturerid
        viewController.title = "New Item"
        
        self.present(navigationController, animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        self.tableView.reloadData()
        updateStatusButton()
    }
    
    
    func loadItems() {
        items = getItems(categoryId: categoryid, accountID: accountid, manufacturerId: manufacturerid, replace: replace, sort: sort)

        totalCosts = 0.0
        
        for item : AnyObject in items {
            if (item.value(forKey: "monthlycosts") != nil ) {
                totalCosts += item.value(forKey: "monthlycosts") as! Double
            }
        }
    }
    
    func updateStatusButton () {
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        var sortOrder: String
        if(sort == "bydate") {
            sortOrder = "Replacement Date"
        }
        else if(sort == "bycosts") {
            sortOrder = "Monthly Costs"
        }
        else if(sort == "bycategory") {
            sortOrder = "Category"
        }
        else if(sort == "bymanufacturer") {
            sortOrder = "Manufacturer"
        }
        else if(sort == "byaccount") {
            sortOrder = "Account"
        }
        else if(sort == "bydepreciation") {
            sortOrder = "Value Loss"
        }
        else {
            sortOrder = ""
        }
        
        statusButton.setTitle(String(format: "%d Items\nMonthly Value Loss %@\nSorted by %@", items.count, formatter.string(from: NSNumber(value:totalCosts))!, sortOrder), for: UIControlState())
        statusButton.sizeToFit()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell

        // Configure the cell...

        let item = items[(indexPath as NSIndexPath).row]
        cell.titleLabel.text = item.value(forKey: "name") as! String?
        
        var celltext : String = ""
        var celltext2: String = ""
        var celltext3: String = ""

        let monthlycosts = item.value(forKey: "monthlycosts") as! Double?

        if (sort == "bycategory") {
            let categoryid = item.value(forKey: "categoryid") as! String?
            if (categoryid != nil) {
                if categoryid != previousId {
                    let group = getCategory(categoryid!)
                    groupname = (group?.value(forKey: "name") as! String?)!
                    previousId = categoryid!
                }
                celltext = groupname
            } else {
                celltext = "No category selected"
            }
        } else if(sort == "bycosts") {
            celltext = NSString(format: "Monthly Cost %.2f", monthlycosts!) as String
        } else if (sort == "byaccount") {
            let accountid = item.value(forKey: "accountid") as! String?
            if (accountid != nil) {
                if accountid != previousId {
                    let group = getAccount(accountid!)
                    groupname = (group?.value(forKey: "name") as! String?)!
                    previousId = accountid!
                }
                celltext = groupname
            } else {
                celltext = "No account selected"
            }
        } else if (sort == "bydepreciation") {
            let depreciation = item.value(forKey: "depreciation") as! Double
            let replace = item.value(forKey: "replace") as! Int?
            if(replace != nil && replace!>0) {
                celltext = String(format: "Depreciation %.2f ", depreciation) as String
            } else {
                celltext = "NA"
            }
        } else if (sort == "bymanufacturer") {
            let manufacturerid = item.value(forKey: "manufacturerid") as! String?
            if (manufacturerid != nil) {
                if manufacturerid != previousId {
                    let group = getManufacturer(manufacturerid!)
                    groupname = (group?.value(forKey: "name") as! String?)!
                    previousId = manufacturerid!
                }
                celltext = groupname
            } else {
                celltext = "No manufacturer selected"
            }
        } else {
            let retirement = item.value(forKey: "retirement") as! Date?
            var retirementtext : String = ""
            if retirement != nil{
                retirementtext = DateFormatter.localizedString(from: retirement!, dateStyle: .short, timeStyle: .none)
            }
            else {
                retirementtext = "NA"
            }
            let replace = item.value(forKey: "replace") as! Int?
            if(replace != nil && replace!>0 && retirementtext != "NA") {
                celltext = NSString(format: "Replace at %@", retirementtext) as String
            } else if(replace != nil && replace!>0 && retirementtext == "NA")  {
                celltext = "Retirement date not entered"
            } else {
                celltext = "Not replaced"
            }
        }

//    } else if (sort == "bymanufacturer") {
        //let manufacturer : NSManagedObject? = nil
        /*var manufacturername : String = ""
        let manufacturerid = item.value(forKey: "manufacturerid") as! String?
        if (manufacturerid != nil) {
            let manufacturer : NSManagedObject = getManufacturer(manufacturerid!)!
            manufacturername = (manufacturer.value(forKey: "name") as! String?)!
        }
        else {
            manufacturername = "Unknown Manufacturer"
        }*/
        var model : String = ""
        if(item.value(forKey: "model") != nil) {
            model = (item.value(forKey: "model") as! String?)!
        }
        if (model.count == 0) {
            model = "No model entered"
        }
        
        if(sort != "bycosts") {
            celltext3 = String(format: "%@", model)
            celltext2 = NSString(format: "Monthly Cost %.2f", monthlycosts!) as String
        }
        else {
            celltext2 = String(format: "%@", model)
        }
        
        cell.subtitleLabel.text = celltext
        cell.subtitle2Label.text = celltext2
        cell.subtitle3Label.text = celltext3
        cell.itemImageView.image = getItemImage(itemid: item.value(forKey: "id") as! String)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        // open action detail view
        let storyboard = UIStoryboard(name: "Item", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ItemDetailView") as! ItemDetailViewController
        //let navigationController = UINavigationController(rootViewController: vc)
        var item: NSManagedObject
        
        //if search {
        //    item = filteredTableData[(indexPath as NSIndexPath).row]
        //} else {
            item = items[(indexPath as NSIndexPath).row]
        //}
        vc.item = item
        vc.title = "Edit Item"
        vc.delegate = self
        
        //resultSearchController.isActive = false
        self.navigationController!.pushViewController(vc, animated: true)
        
        //self.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteClosure = { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
            let itemToDelete = self.items[indexPath.row]
            
            self.deleteItem(itemToDelete)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        /*
         let editClosure = { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
         // open project detail view
         let storyboard = UIStoryboard(name: "Project", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "ProjectDetailView") as! ProjectDetailViewController
         let navigationController = UINavigationController(rootViewController: vc)
         vc.project = self.items[indexPath.row]
         vc.title = "Project"
         vc.delegate = self
         
         self.present(navigationController, animated: true, completion: nil)
         }*/
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: deleteClosure)
        //        let editAction = UITableViewRowAction(style: .normal, title: "Edit", handler: editClosure)
        //        let summaryAction = UITableViewRowAction(style: .normal, title: "Summary", handler: summaryClosure)
        
        //        summaryAction.backgroundColor = UIColor.darkGray
        
        return [deleteAction]
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    func deleteItemDelegate(_ controller: ItemDetailViewController) {
        deleteItem(controller.item)
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewController(animated: true)
    }
    
    @objc func deleteItem(_ itemToDelete:NSManagedObject)
    {
        undoManager.registerUndo(withTarget: self, selector:#selector(ItemsViewController.deleteItem(_:)), object:itemToDelete)
        undoManager.setActionName("delete item")
        
        // Delete it from the managedObjectContext
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        if self.undoer.isUndoing {
            itemToDelete.setValue(false, forKey: "trash")
        }
        else
        {
            itemToDelete.setValue(true, forKey: "trash")
        }
        
        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }
        
        // Refresh the table view to indicate that it's deleted
        self.loadItems()
        
        if self.undoer.isUndoing || self.undoer.isRedoing {
            self.tableView.reloadData()
        }
    }
}
