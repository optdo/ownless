//
//  ImageCell.swift
//  OwnLess
//
//  Created by Bastiaan on 25/06/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit

class ItemCell : UITableViewCell {
        
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var subtitleLabel: UILabel!
    @IBOutlet var subtitle2Label: UILabel!
    @IBOutlet var subtitle3Label: UILabel!
    
}
