//
//  ImageCollectionViewController.swift
//  OwnLess
//
//  Created by Bastiaan on 25/06/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData


final class ImageCollectionViewController: UICollectionViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UICollectionViewDelegateFlowLayout  {
    
    var imagePickerController : UIImagePickerController!
    
    // MARK: - Properties
    fileprivate let itemsPerRow: CGFloat = 3
    fileprivate let reuseIdentifier = "ImageCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    var itemId : String = ""
    var type : String = "item"
    
    var images = [NSManagedObject]()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        var toolBarItems = [UIBarButtonItem]()
        
        let systemButton1 = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(ImageCollectionViewController.addImage(_:)))

        toolBarItems.append(systemButton1)
        
        self.navigationController?.isToolbarHidden = false
        self.setToolbarItems(toolBarItems as [UIBarButtonItem], animated: true)
        
        if type == "receipt" {
            self.title = "Receipt Images"
        }
        else {
            self.title = "Item Images"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        images = getImages(itemid: itemId, type: type)
        self.collectionView?.reloadData()
    }
    

    
    @objc func addImage(_ sender: Any) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = .camera
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePickerController.dismiss(animated: true, completion: nil)
 //       imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage

        if image != nil {
            // create NSData from UIImage
            guard let imageData = UIImageJPEGRepresentation(image!, 1) else {
                // handle failed conversion
                print("jpg error")
                return
            }
            //1
            let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
            //2
            
            // new item
            let entity =  NSEntityDescription.entity(forEntityName: "Image", in: managedContext)
            let imaged = NSManagedObject(entity: entity!, insertInto:managedContext)
            let uuid = UUID().uuidString
            imaged.setValue(uuid, forKey: "id")
            
            // 3
            imaged.setValue(itemId, forKey: "itemid")
            imaged.setValue(imageData, forKey: "imagedata")
            imaged.setValue(NSDate(), forKey: "lastupdate")
            imaged.setValue(type, forKey: "type")
            
            //4
            var error: NSError?
            do {
                try managedContext.save()
            } catch let error1 as NSError {
                error = error1
                print("Could not save \(error), \(error?.userInfo)")
            }
            
            self.collectionView?.reloadData()
        }
    }
    
    
    // MARK: - Private
//    func photoForIndexPath(indexPath: IndexPath) -> FlickrPhoto {
//        return images[(indexPath as NSIndexPath).section].searchResults[(indexPath as IndexPath).row]
//    }

    //1
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    override func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    //3
    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! ImageCell
        cell.backgroundColor = UIColor.black
        // Configure the cell
        
        //3
        let item = images[(indexPath as NSIndexPath).row]
        let imageData = item.value(forKey: "imagedata")
        
        let image = UIImage(data: imageData as! Data)
        cell.imageView.image = image 
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowImageDetail" {
            let imageViewController = segue.destination as! ImageDetailViewController
            imageViewController.type = type
            //var point : CGPoint = sender.convertPoint(CGPointZero, toView:collectionView)
            //ar indexPath = collectionView!.indexPathForItem(at: point)
            
            //guard let cell = self..superview as? ImageCell else {
            //    return // or fatalError() or whatever
            //}
            //let indexPath = self.collectionView.indexPath(for: cell)
            //var visibleRect = CGRect()
            //visibleRect.origin = (collectionView?.contentOffset)!
            //visibleRect.size = (collectionView?.bounds.size)!
            
            //let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            
            //guard let indexPath = collectionView?.indexPathForItem(at: visiblePoint) else { return }

            let selectedCell = sender as! ImageCell
            let indexPath = collectionView?.indexPath(for: selectedCell)
            
            //let indexPath: IndexPath = self.collectionView?.indexPathsForSelectedItems.
            let item = images[(indexPath?.row)!]
            let imageData = item.value(forKey: "imagedata")
            
            let image = UIImage(data: imageData as! Data)
            
            imageViewController.imageObject = item
            imageViewController.image = image
        }
    }
}
