//
//  DataIO.swift
//  OwnLess
//
//  Created by Bastiaan on 20/03/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import Foundation

import UIKit
import EventKit
import CoreData

// MARK: Helper Functions

func getItems(categoryId: String?, accountID: String?, manufacturerId: String?, replace: Bool?, sort: String?) -> [NSManagedObject] {
    updateCalculatedValues()
    return getItemsSub(categoryId: categoryId, accountID: accountID, manufacturerId: manufacturerId, replace: replace, sort: sort)
}

func getItemsSub(categoryId: String?, accountID: String?, manufacturerId: String?, replace: Bool?, sort: String?) -> [NSManagedObject] {
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Item")
    
    // Set Sorting
    if sort == "bycosts" {
        let sortDescriptor1 = NSSortDescriptor(key: "monthlycosts", ascending: false)
        let sortDescriptor2 = NSSortDescriptor(key: "replace", ascending: false)
        let sortDescriptor3 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2, sortDescriptor3]
    }
    else if sort == "bycategory" {
        let sortDescriptor1 = NSSortDescriptor(key: "categoryid", ascending: true)
        let sortDescriptor2 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2]
    }
    else if sort == "bymanufacturer" {
        let sortDescriptor1 = NSSortDescriptor(key: "manufacturerid", ascending: true)
        let sortDescriptor2 = NSSortDescriptor(key: "model", ascending: true)
        let sortDescriptor3 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2, sortDescriptor3]
    }
    else if sort == "byaccount" {
        let sortDescriptor1 = NSSortDescriptor(key: "accountid", ascending: true)
        let sortDescriptor2 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2]
    }
    else if sort == "bydepreciation" {
        let sortDescriptor1 = NSSortDescriptor(key: "depreciation", ascending: false)
        let sortDescriptor2 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2]
    }
    else {
        let sortDescriptor1 = NSSortDescriptor(key: "replace", ascending: false)
        let sortDescriptor2 = NSSortDescriptor(key: "retirement", ascending: true)
        let sortDescriptor3 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
        fetchRequest.sortDescriptors = [sortDescriptor1, sortDescriptor2, sortDescriptor3]
    }
    
    // Set Filter
    
    var subPredicates: [NSPredicate]! = []
    var subPredicate = NSPredicate()

    subPredicate = NSPredicate(format: "(trash == false OR trash == nil)")
    subPredicates.append(subPredicate)
    
    if accountID != nil {
        subPredicate = NSPredicate(format: "accountid == %@", accountID!)
        subPredicates.append(subPredicate)
    }
    if categoryId != nil {
        subPredicate = NSPredicate(format: "categoryid == %@", categoryId!)
        subPredicates.append(subPredicate)
    }
    if manufacturerId != nil {
        subPredicate = NSPredicate(format: "manufacturerid == %@", manufacturerId!)
        subPredicates.append(subPredicate)
    }
    if replace != nil && replace == true {
        subPredicate = NSPredicate(format: "replace == true")
        subPredicates.append(subPredicate)
        subPredicate = NSPredicate(format: "retirement < %@", NSDate())
        subPredicates.append(subPredicate)
    }

    // Set the predicate on the fetch request
    fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: subPredicates)
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    
    return results
}

func getImages(itemid: String, type: String) -> [NSManagedObject] {
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Image")
    
    let sortDescriptor1 = NSSortDescriptor(key: "lastupdate", ascending: true)
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    // Set Filter
    
    var subPredicates: [NSPredicate]! = []
    var subPredicate = NSPredicate()
    
//    subPredicate = NSPredicate(format: "(trash == false OR trash == nil)")
//    subPredicates.append(subPredicate)
    
    subPredicate = NSPredicate(format: "itemid == %@", itemid)
    subPredicates.append(subPredicate)

    subPredicate = NSPredicate(format: "type == %@", type)
    subPredicates.append(subPredicate)
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: subPredicates)
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    
    return results
}

func updateCalculatedValues() {
    let items = getItemsSub(categoryId: nil, accountID: nil, manufacturerId: nil, replace: false, sort: nil)
   
    let today = Date()
    
    for resultItem : AnyObject in items {
        let purchasevalue = resultItem.value(forKey: "purchasevalue") as! Double
        let residualvalue = resultItem.value(forKey: "residualvalue") as! Double
        let costs = resultItem.value(forKey: "monthlycosts") as! Double
        
        var acquired : Date! = nil
        var depreciation : Double = 0
        acquired = resultItem.value(forKey: "acquired") as! Date?
        let retirement = resultItem.value(forKey: "retirement") as! Date?
        let replace = resultItem.value(forKey: "replace") as! Bool?
        
        if (replace != nil && replace! && retirement != nil && retirement! > today) {
            let dateComponents =  Calendar.current.dateComponents([.weekOfYear, .month], from: acquired!, to: today)
            let months = dateComponents.month
            
            depreciation = Double(months!) * costs
        } else if (replace != nil && replace! && retirement != nil)
        {
            depreciation = (purchasevalue - residualvalue)
        }
        
        // Update Item
        resultItem.setValue(depreciation, forKey: "depreciation")
        resultItem.setValue(Date(), forKey: "lastcalc")

        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }
    }
}


func saveItem(_ item: NSManagedObject!, completed : Bool, oldprojectid : String?, oldcontextid : String?) {
    
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    if (completed)
    {
        item.setValue(true, forKey: "completed")
        item.setValue(Date(), forKey: "completeddate")
    }
    
    // Check for repeating action
    var repeatEvery : Int = 1
    var repeatFrequency : Int = 0
    
    if (item.value(forKey: "repeatfrequency") != nil)
    {
        repeatEvery = item.value(forKey: "repeatevery") as! Int
        repeatFrequency = item.value(forKey: "repeatfrequency") as! Int
    }
    
    if completed && repeatFrequency>0 {
        
        let entity =  NSEntityDescription.entity(forEntityName: "Action",
                                                 in:
            managedContext)
        
        let newItem = NSManagedObject(entity: entity!,
                                      insertInto:managedContext)
        
        let attributes = newItem.entity.attributesByName
        for attributeName in attributes.keys  {
            
            let value: AnyObject? = item.value(forKey: attributeName) as AnyObject?
            if value != nil {
                newItem.setValue(value, forKey: attributeName)
            }
        }
        
        let existingduedate : Date = item.value(forKey: "duedate") as! Date
        var existingstartdate : Date? = nil
        if (item.value(forKey: "startdate") != nil)
        {
            existingstartdate = item.value(forKey: "startdate") as! Date?
        }
        
        var duedate : Date = Date()
        var startdate : Date = Date()
        switch (repeatFrequency) {
        case (1):
            duedate = Calendar.current.date(byAdding: .day, value: repeatEvery,  to:duedate)!
        case (2):
            duedate = Calendar.current.date(byAdding: .weekOfYear, value: repeatEvery,  to:duedate)!
        case (3):
            duedate = Calendar.current.date(byAdding: .month, value: repeatEvery,  to:duedate)!
        case (4):
            duedate = Calendar.current.date(byAdding: .year, value: repeatEvery,  to:duedate)!
        default:
            duedate = Date()
        }
        if (existingstartdate != nil) {
            let interval = existingstartdate!.timeIntervalSince(existingduedate)
            startdate = duedate.addingTimeInterval(interval)
            newItem.setValue(startdate, forKey: "startdate")
        }
        
        // Override with new due date and id
        newItem.setValue(duedate, forKey: "duedate")
        let uuid = UUID().uuidString
        newItem.setValue(uuid, forKey: "id")
        //println(newItem.valueForKey("name"))
        newItem.setValue(false, forKey: "completed")
        newItem.setValue(nil, forKey: "completeddate")
    }
    
    // Save data
    var error: NSError?
    do {
        try managedContext.save()
    } catch let error1 as NSError {
        error = error1
        print("Could not save \(error), \(error?.userInfo)")
        /*let alert = UIAlertView()
         alert.title = "Error"
         alert.message = "Failed to save action"
         alert.addButton(withTitle: "Continue")
         alert.show()*/
    }
    
    
    
    // Todo: Update totals
}

func updateCategoryTotals(_ categoryId: String)
{
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    //2
    //var fetchRequest = NSFetchRequest(entityName:"Action")
    var fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Item")
    
    var predicate = NSPredicate(format: "categoryid == %@", categoryId)
    fetchRequest.predicate = predicate
    
    //3
    var items = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        items = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    
    var totalitems : Int = 0
    var totalpurchasevalue : Double = 0
    var totalcosts : Double = 0
    var totaldepreciation : Double = 0
//    var totaltime : TimeInterval = 0

    for resultItem : AnyObject in items {
//        var time : TimeInterval = 0
        let purchasevalue = resultItem.value(forKey: "purchasevalue") as! Double
        let costs = resultItem.value(forKey: "monthlycosts") as! Double
        let depreciation = resultItem.value(forKey: "depreciation") as! Double

        totalitems += 1
        totalcosts += costs
        totalpurchasevalue += purchasevalue
        totaldepreciation += depreciation
    }
    // Get category
    
    //2
    fetchRequest = NSFetchRequest(entityName:"Category")
    
    predicate = NSPredicate(format: "id == %@", categoryId)
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    var cats = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        cats = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    if (cats.count==1)
    {
        let cat = cats[0]
        
        cat.setValue(totalitems, forKey: "items")
        cat.setValue(totalpurchasevalue, forKey: "purchasevalue")
        cat.setValue(totalcosts, forKey: "monthlycosts")
        cat.setValue(totaldepreciation, forKey: "depreciation")

        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }
    }
}

func loadAccounts() -> [NSManagedObject] {
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    //2
    //let fetchRequest = NSFetchRequest(entityName:"Project")
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Account")
    
    let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    var predicate = NSPredicate()
  
    predicate = NSPredicate(format: "(trash == false OR trash == nil)")
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    return results
}

func loadCategories() -> [NSManagedObject] {
    
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    //2
    //let fetchRequest = NSFetchRequest(entityName:"Calendar")
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Category")
    
    let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    var predicate: NSPredicate
    predicate = NSPredicate(format: "(trash == false OR trash == nil)")

    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    return results
}


func loadManufactures() -> [NSManagedObject] {
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    //2
    //let fetchRequest = NSFetchRequest(entityName:"Timeslot")
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Manufacturer")
    
    let predicate = NSPredicate(format: "(trash == false OR trash == nil)")
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
    
    // Set the list of sort descriptors in the fetch request,
    // so it includes the sort descriptor
    fetchRequest.sortDescriptors = [sortDescriptor]
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    return results
}

func loadItem(_ itemId: String) -> NSManagedObject? {
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Item")
    
    let predicate = NSPredicate(format: "id == '%@'", itemId)
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
    
    // Set the list of sort descriptors in the fetch request,
    // so it includes the sort descriptor
    fetchRequest.sortDescriptors = [sortDescriptor]
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    
    if results.count > 0 {
        return results.first
    }
    else {
        return nil
    }
}

func getCategory(_ id: String) -> NSManagedObject? {
    
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Category")

    //let predicate = NSPredicate(format: "id == '%@'", id)
    let predicate = NSPredicate(format: "id == %@", id)

    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate

    let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
    fetchRequest.sortDescriptors = [sortDescriptor1]
        
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    if results.count > 0 {
        return results.first
    }
    else {
        return nil
    }
}

func getManufacturer(_ id: String) -> NSManagedObject? {
    
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Manufacturer")
    
    //let predicate = NSPredicate(format: "id == '%@'", id)
    let predicate = NSPredicate(format: "id == %@", id)
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    if results.count > 0 {
        return results.first
    }
    else {
        return nil
    }
}

func getAccount(_ id: String) -> NSManagedObject? {
    
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Account")
    
    //let predicate = NSPredicate(format: "id == '%@'", id)
    let predicate = NSPredicate(format: "id == %@", id)
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    if results.count > 0 {
        return results.first
    }
    else {
        return nil
    }
}

func getItemImage(itemid: String) -> UIImage? {
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Image")
    
    let predicate = NSPredicate(format: "itemid == %@ and type == 'item'", itemid)
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    let sortDescriptor1 = NSSortDescriptor(key: "lastupdate", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    if results.count > 0 {
        let imageData = results.first?.value(forKey: "imagedata")
        
        return UIImage(data: imageData as! Data)
    }
    else {
        return nil
    }
}

func getReceiptImage(itemid: String) -> UIImage? {
    //1
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //2
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Image")
    
    let predicate = NSPredicate(format: "itemid == %@ and type == 'receipt'", itemid)
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    let sortDescriptor1 = NSSortDescriptor(key: "lastupdate", ascending: true, selector: #selector(NSString.caseInsensitiveCompare(_:)))
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    //3
    var results = [NSManagedObject]()
    
    do {
        let fetchedResults =
            try managedContext.fetch(fetchRequest) as! [NSManagedObject]
        results = fetchedResults
    }
    catch let error as NSError {
        NSLog("%@", error.localizedDescription)
    }
    catch {
        fatalError("Fetching from the store failed")
    }
    if results.count > 0 {
        let imageData = results.first?.value(forKey: "imagedata")
        
        return UIImage(data: imageData as! Data)
    }
    else {
        return nil
    }
}


