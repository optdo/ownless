//
//  CategoryDetailViewController.swift
//  OwnLess
//
//  Created by Bastiaan on 04/03/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

protocol CategoryDetailDelegate{
    func deleteCategory(_ controller:CategoryDetailViewController)
    func selectCategory(_ controller:CategoryDetailViewController)
}

class CategoryDetailViewController: UITableViewController {

    var delegate:CategoryDetailDelegate! = nil
    var category : NSManagedObject!
    var categoryId : String = ""
    
    //MARK: Properties
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var costLabel: UILabel!
    @IBOutlet var depreciatedLabel: UILabel!
    @IBOutlet var purchaseValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()

        if (category != nil)
        {
            nameTextField.text = category.value(forKey: "name") as! String?
            purchaseValueLabel.text = String(format: "%.2f", (category.value(forKey: "purchasevalue") as! Double?)!)
            costLabel.text = String(format: "%.2f", (category.value(forKey: "monthlycosts") as! Double?)!)
            depreciatedLabel.text = String(format: "%.2f", (category.value(forKey: "depreciation") as! Double?)!)
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.category == nil) {
            return 1
        } else {
            return 1
        }
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func done(_ sender: UIBarButtonItem) {
        // Save Account
        
        //1
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        if(self.category == nil)
        {
            //2
            let entity =  NSEntityDescription.entity(forEntityName: "Category",
                                                     in:
                managedContext)
            
            let item = NSManagedObject(entity: entity!,
                                       insertInto:managedContext)
            
            //3
            item.setValue(self.nameTextField.text, forKey: "name")
            let uuid = UUID().uuidString
            item.setValue(uuid, forKey: "id")
            //            item.setValue(false, forKey: "trash")
            
            //4
            var error: NSError?
            do {
                try managedContext.save()
            } catch let error1 as NSError {
                error = error1
                print("Could not save \(error), \(error?.userInfo)")
            }
            self.categoryId = uuid
        }
        else
        {
            let item = self.category;
            
            item?.setValue(self.nameTextField.text, forKey: "name")
            
            var error: NSError?
            do {
                try managedContext.save()
            } catch let error1 as NSError {
                error = error1
                print("Could not save \(error), \(error?.userInfo)")
            }
        }

        self.delegate?.selectCategory(self)

        self.dismiss(animated: true, completion: nil)
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
