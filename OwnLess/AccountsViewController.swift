//
//  AccountViewController.swift
//  OwnLess
//
//  Created by Bastiaan on 04/03/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class AccountsViewController: UITableViewController, AccountDetailDelegate {

    fileprivate var statusButton: UIButton = UIButton()
    
    var accounts = [NSManagedObject]()

    let undoer = UndoManager()

    override var undoManager : UndoManager {
        get {
            return self.undoer
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accounts.count
    }
    
    @IBAction func add(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AccountDetailView") as! AccountDetailViewController
        
        let navigationController = UINavigationController(rootViewController: viewController)

        viewController.title = "New Account"
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func addItem(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Item", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ItemDetailView") as! ItemDetailViewController
        
        let navigationController = UINavigationController(rootViewController: viewController)
        
        viewController.title = "New Item"
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        self.tableView.reloadData()

        var toolBarItems = [UIBarButtonItem]()
        
        let totalItems = String(format: "%d Accounts", accounts.count)
        
        let systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let systemButton2 = UIBarButtonItem(title: totalItems, style: .plain, target: self, action: nil)
        
        let systemButton3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
 
        let systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.compose, target: self, action: #selector(AccountsViewController.addItem(_:)))

        statusButton.titleLabel?.numberOfLines = 1
        statusButton.setTitleColor(UIColor.gray, for: UIControlState())
        statusButton.titleLabel!.font =  UIFont.systemFont(ofSize: 13.0)
        statusButton.titleLabel!.textAlignment = .center
        statusButton.setTitle(totalItems, for: UIControlState())
        statusButton.sizeToFit()
        systemButton2.customView = statusButton
        
        toolBarItems.append(systemButton1)
        toolBarItems.append(systemButton2)
        toolBarItems.append(systemButton3)
        toolBarItems.append(systemButton4)
        
        self.navigationController?.isToolbarHidden = false
        self.setToolbarItems(toolBarItems as [UIBarButtonItem], animated: true)
    }
    
    func loadItems() {
        accounts = loadAccounts()
    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountCell", for: indexPath)

        // Configure the cell...

        let item = accounts[(indexPath as NSIndexPath).row]
        cell.textLabel!.text = item.value(forKey: "name") as! String?

        return cell
    }

    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteClosure = { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
            let itemToDelete = self.accounts[indexPath.row]
            
            self.deleteItem(itemToDelete)
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        }

        let editClosure = { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
            // open project detail view
            let storyboard = UIStoryboard(name: "Account", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AccountDetailView") as! AccountDetailViewController
            let navigationController = UINavigationController(rootViewController: vc)
            vc.account = self.accounts[indexPath.row]
            vc.title = "Edit Account"
            vc.delegate = self
            
            self.present(navigationController, animated: true, completion: nil)
        }
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: deleteClosure)
        let editAction = UITableViewRowAction(style: .normal, title: "Edit", handler: editClosure)
//        let summaryAction = UITableViewRowAction(style: .normal, title: "Summary", handler: summaryClosure)
        
//        summaryAction.backgroundColor = UIColor.darkGray
        
        return [deleteAction, editAction]
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        // open project detail view
        let storyboard = UIStoryboard(name: "Account", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AccountDetailView") as! AccountDetailViewController
        let navigationController = UINavigationController(rootViewController: vc)
        vc.account = self.accounts[indexPath.row]
        vc.title = "Edit Account"
        vc.delegate = self
        
        self.present(navigationController, animated: true, completion: nil)
    }

    func deleteAccount(_ controller: AccountDetailViewController) {
        
        deleteItem(controller.account)
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewController(animated: true)
    }
    
    func selectAccount(_ controller: AccountDetailViewController) {
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewController(animated: true)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
         if segue.identifier == "ShowItemsFromAccounts" {
             let indexPath: IndexPath! = self.tableView.indexPathForSelectedRow
             let item = accounts[indexPath.row]
             let vc = segue.destination as! ItemsViewController
             vc.accountid = item.value(forKey: "id") as! String
         }
     }

    @objc func deleteItem(_ itemToDelete:NSManagedObject)
    {
        undoManager.registerUndo(withTarget: self, selector:#selector(AccountsViewController.deleteItem(_:)), object:itemToDelete)
        undoManager.setActionName("delete account")
        
        // Delete it from the managedObjectContext
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        if self.undoer.isUndoing {
            itemToDelete.setValue(false, forKey: "trash")
        }
        else
        {
            itemToDelete.setValue(true, forKey: "trash")
        }
        
        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }
        
        // Refresh the table view to indicate that it's deleted
        self.loadItems()
        
        if self.undoer.isUndoing || self.undoer.isRedoing {
            self.tableView.reloadData()
        }
    }
}
