//
//  ReceiptViewController.swift
//  OwnLess
//
//  Created by Bastiaan on 18/05/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class ImageViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

    var imagePickerController : UIImagePickerController!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var selectLabel: UIButton!
    
    var image: UIImage! = nil
    var type: String! = nil

    @IBAction func imageButton(_ sender: Any) {
        imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self 
        imagePickerController.sourceType = .camera
        present(imagePickerController, animated: true, completion: nil)
    }

    
    @IBAction func deleteButton(_ sender: Any) {
        image = nil
        imageView.image = nil
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePickerController.dismiss(animated: true, completion: nil)
        imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        //get the image we took with camera
        let image = imageView.image!
        //get the PNG data for this image
        let data = UIImagePNGRepresentation(image)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if image != nil {
            imageView.image = image
            selectLabel.titleLabel?.text = "Replace"
        }
        if type == "receipt" {
            self.title = "Receipt Image"
        }
        else {
            self.title = "Item Image"
        }
    }
}
