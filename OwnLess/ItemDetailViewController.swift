//
//  ItemDetailViewController.swift
//  OwnLess
//
//  Created by Bastiaan on 04/03/2018.
//  Copyright © 2018 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

protocol ItemDetailDelegate{
    func deleteItemDelegate(_ controller:ItemDetailViewController)
}

class ItemDetailViewController: UITableViewController {

    var delegate:ItemDetailDelegate! = nil
    
    var item : NSManagedObject!
    var acquired: Date! = nil
    var retirement: Date! = nil
    var accountid : String! = nil
    var categoryid : String! = nil
    var manufacturerid : String! = nil
    var warranty: Date! = nil
    //var itemImage: UIImage? = nil
    //var receiptImage: UIImage? = nil

    //MARK: Properties
    @IBOutlet var itemImage: UIImageView!
    @IBOutlet var receiptImage: UIImageView!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var acquiredTextField: UITextField!
    @IBOutlet var retirementTextField: UITextField!
    @IBOutlet var replaceSegmentedControl: UISegmentedControl!
    @IBOutlet var purchaseValueTextField: UITextField!
    @IBOutlet var restValue: UITextField!
    @IBOutlet var accountLabel: UILabel!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var manufacturerLabel: UILabel!
    @IBOutlet var modelTextField: UITextField!
    @IBOutlet var serialTextField: UITextField!
    @IBOutlet var warrantyTextField: UITextField!
    @IBOutlet var receiptLabel: UILabel!
    @IBOutlet var imageLabel: UILabel!
    @IBOutlet var lenderLabel: UILabel!
    
    @IBAction func acquiredEditingDidBegin(_ sender: UITextField) {
        let datePickerViewStart  : UIDatePicker = UIDatePicker()
        datePickerViewStart.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerViewStart
        datePickerViewStart.addTarget(self, action: #selector(ItemDetailViewController.datePickerValueChanged(_:)), for: UIControlEvents.valueChanged)
        if acquired == nil {
            acquired = Date()
            
            let dateformatter = DateFormatter()
            dateformatter.setLocalizedDateFormatFromTemplate("MMMddyyyyEEE")
            let dateString = dateformatter.string(from: acquired)
            acquiredTextField.text = "\(dateString)"
        }
        //if acquired != nil {
        datePickerViewStart.date = acquired
        //}
    }
    @IBAction func acquiredEditingDidEnd(_ sender: UITextField) {
        let dateformatter = DateFormatter()
        dateformatter.setLocalizedDateFormatFromTemplate("MMMddyyyyEEE")
        // To Do add 2 years by default
        
        var timeInterval = DateComponents()
        timeInterval.month = 24
        timeInterval.day = 1
        let futureDate = Calendar.current.date(byAdding: timeInterval, to: acquired)
        
        let dateString = dateformatter.string(from: futureDate!)
        if(retirement == nil) {
            retirementTextField.text = "\(dateString)"
            retirement = futureDate
        }
        if(warranty == nil) {
            warrantyTextField.text = "\(dateString)"
            warranty = futureDate
        }
    }
    
    @IBAction func retirementEditingDidBegin(_ sender: UITextField) {
        let datePickerViewStart  : UIDatePicker = UIDatePicker()
        datePickerViewStart.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerViewStart
        datePickerViewStart.addTarget(self, action: #selector(ItemDetailViewController.datePickerValueChanged2(_:)), for: UIControlEvents.valueChanged)
        if retirement != nil {
            datePickerViewStart.date = retirement
        }
    }
    
    @IBAction func warrantyEditingDidBegin(_ sender: UITextField) {
        let datePickerViewStart  : UIDatePicker = UIDatePicker()
        datePickerViewStart.datePickerMode = UIDatePickerMode.date
        sender.inputView = datePickerViewStart
        datePickerViewStart.addTarget(self, action: #selector(ItemDetailViewController.datePickerValueChanged3(_:)), for: UIControlEvents.valueChanged)
        if warranty != nil {
            datePickerViewStart.date = warranty
        }
    }
    
    @IBAction func addItem(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Item", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ItemDetailView") as! ItemDetailViewController
        
        let navigationController = UINavigationController(rootViewController: viewController)
        
        viewController.title = "New Item"
        
        self.present(navigationController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

        if (item == nil) {
        //1
            var item: NSManagedObject
            
            //2

            // new item
            let entity =  NSEntityDescription.entity(forEntityName: "Item", in: managedContext)
            item = NSManagedObject(entity: entity!, insertInto:managedContext)
            let uuid = UUID().uuidString
            item.setValue(uuid, forKey: "id")
            
            self.item = item
            
            // Set focus on name field
            self.nameTextField.updateFocusIfNeeded()
        }
        else
        {
            let dateformatter = DateFormatter()
            dateformatter.setLocalizedDateFormatFromTemplate("MMMddyyyyEEE")
            
            nameTextField.text = item.value(forKey: "name") as! String?
            
            accountid = item.value(forKey: "accountid") as! String?
            categoryid = item.value(forKey: "categoryid") as! String?
            manufacturerid = item.value(forKey: "manufacturerid") as! String?

            modelTextField.text = item.value(forKey: "model") as! String?
            serialTextField.text = item.value(forKey: "serial") as! String?

            acquired = item.value(forKey: "acquired") as! Date?
            if acquired != nil {
                self.acquiredTextField.text = dateformatter.string(from: acquired)
            }
            retirement = item.value(forKey: "retirement") as! Date?
            if retirement != nil {
                self.retirementTextField.text = dateformatter.string(from: retirement)
            }
            let replace = item.value(forKey: "replace") as! Bool?
            if(replace != nil && replace!) {
                replaceSegmentedControl.selectedSegmentIndex = 0
            } else {
                replaceSegmentedControl.selectedSegmentIndex = 1
            }
            
            let purchasevalue = item.value(forKey: "purchasevalue") as! Float?
            if purchasevalue != nil {
                self.purchaseValueTextField.text =  NSString(format: "%.2f", purchasevalue!) as String
            }
            let residualvalue = item.value(forKey: "residualvalue") as! Float?
            if residualvalue != nil {
                self.restValue.text =  NSString(format: "%.2f", residualvalue!) as String
            }
            warranty = item.value(forKey: "warrantyexpiration") as! Date?
            if warranty != nil {
                self.warrantyTextField.text = dateformatter.string(from: warranty)
            }
            var toolBarItems = [UIBarButtonItem]()
            
            //        var systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Bookmarks, target: nil, action: nil)
            //        toolBarItems.addObject(systemButton1)
            
            let systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            toolBarItems.append(systemButton2)
            
            let systemButton3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(ItemDetailViewController.deleteItemDelegate(_:)))
            toolBarItems.append(systemButton3)
            
            let systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            toolBarItems.append(systemButton4)
            
            let systemButton5 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.compose, target: self, action: #selector(ItemDetailViewController.addItem(_:)))
            toolBarItems.append(systemButton5)
            
            self.navigationController?.isToolbarHidden = false
            self.setToolbarItems(toolBarItems, animated: true)
        }
        
        if(accountid != nil)
        {
            let account = getAccount(accountid)
            
            if (account != nil) {
                self.accountLabel.text = account?.value(forKey: "name") as! String?
            }
        }
        if(categoryid != nil)
        {
            let category = getCategory(categoryid)
            
            if (category != nil) {
                self.categoryLabel.text = category?.value(forKey: "name") as! String?
            }
        }
        if(manufacturerid != nil)
        {
            let manufacturer = getManufacturer(manufacturerid)
            
            if (manufacturer != nil) {
                self.manufacturerLabel.text = manufacturer?.value(forKey: "name") as! String?
            }
        }
        
        itemImage.image = getItemImage(itemid: item.value(forKey: "id") as! String)
        receiptImage.image = getReceiptImage(itemid: item.value(forKey: "id") as! String)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 13
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
        
        // New item was cancelled
        if (item.value(forKey: "replace") == nil)
        {
            // Delete it from the managedObjectContext
            let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            
            managedContext.delete(item)
            
            // To do: Delete any linked images
            
            var error: NSError?
            do {
                try managedContext.save()
            } catch let error1 as NSError {
                error = error1
                print("Could not save \(error), \(error?.userInfo)")
            }

        }
    }

    @IBAction func done(_ sender: UIBarButtonItem) {
        // Save Item
        
        //1
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        var item: NSManagedObject
        
        //2
        if(self.item == nil)
        {
            // new item
            let entity =  NSEntityDescription.entity(forEntityName: "Item", in: managedContext)
            item = NSManagedObject(entity: entity!, insertInto:managedContext)
            let uuid = UUID().uuidString
            item.setValue(uuid, forKey: "id")
        }
        else
        {
            // update item
            item = self.item;
        }
        
        //3
        item.setValue(self.nameTextField.text, forKey: "name")
        //            item.setValue(false, forKey: "trash")
        if(self.acquired != nil) {
            item.setValue(self.acquired, forKey: "acquired")
        }
        if(self.retirement != nil) {
            item.setValue(self.retirement, forKey: "retirement")
        }
        var replace: Bool
        if (self.replaceSegmentedControl.selectedSegmentIndex == 1) {
            replace = false }
        else {
            replace = true }
        item.setValue(replace, forKey: "replace")
        if(self.purchaseValueTextField.text != nil)  && (self.purchaseValueTextField.text != "") {
            item.setValue((self.purchaseValueTextField.text as! NSString).floatValue, forKey: "purchasevalue")
        }
        if(self.restValue.text != nil)  && (self.restValue.text != "") {
            item.setValue((self.restValue.text as! NSString).floatValue, forKey: "residualvalue")
        }
        if(self.accountid != nil){
            item.setValue(self.accountid, forKey: "accountid")
        }
        if(self.categoryid != nil){
            item.setValue(self.categoryid, forKey: "categoryid")
        }
        if(self.manufacturerid != nil){
            item.setValue(self.manufacturerid, forKey: "manufacturerid")
        }
        item.setValue(self.modelTextField.text, forKey: "model")
        item.setValue(self.serialTextField.text, forKey: "serial")
        if(self.warranty != nil) {
            item.setValue(self.warranty, forKey: "warrantyexpiration")
        }
        /*if itemImage != nil {
            // create NSData from UIImage
            guard let imageData = UIImageJPEGRepresentation(itemImage!, 1) else {
                // handle failed conversion
                print("jpg error")
                return
            }
            item.setValue(imageData, forKey: "image")
        }
        if receiptImage != nil {
            // create NSData from UIImage
            guard let imageData = UIImageJPEGRepresentation(receiptImage!, 1) else {
                // handle failed conversion
                print("jpg error")
                return
            }
            item.setValue(imageData, forKey: "receipt")
        }*/
        // Calculate monthly costs
        let purchaseValue = (self.purchaseValueTextField.text as! NSString).floatValue
        let residualValue = (self.restValue.text as! NSString).floatValue
        var depreciation : Float = 0
        if (purchaseValue > 0)
        {
            let acquired = self.acquired
            let retired = self.retirement
            
            if (replace && acquired != nil && retired != nil) {
                let dateComponents =  Calendar.current.dateComponents([.weekOfYear, .month], from: acquired!, to: retired!)
                let months = dateComponents.month
            
                depreciation = (purchaseValue - residualValue) / Float(months!)
            }
        }
        item.setValue(depreciation, forKey: "monthlycosts")
        
        //4
        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }
        
        // Update totals
        if(self.categoryid != nil){
            updateCategoryTotals(self.categoryid)
        }
        
        // Exit
        self.dismiss(animated: true, completion: nil)
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowAccountSelector" {
            let itemViewController = segue.destination as! AccountSelectorViewController
            if (accountid != nil) {
                itemViewController.selectedItemId = self.accountid! as String
            }
        }
        if segue.identifier == "ShowCategorySelector" {
            let itemViewController = segue.destination as! CategorySelectorViewController
            if (categoryid != nil) {
                itemViewController.selectedItemId = self.categoryid! as String
            }
        }
        if segue.identifier == "ShowManufacturerSelector" {
            let itemViewController = segue.destination as! ManufacturerSelectorViewController
            if (manufacturerid != nil) {
                itemViewController.selectedItemId = self.manufacturerid! as String
            }
        }
        if segue.identifier == "ShowItemImages" {
            let itemsViewController = segue.destination as! ImageCollectionViewController
            itemsViewController.type = "item"
            itemsViewController.itemId = (item.value(forKey: "id") as! String?)!
        }
        if segue.identifier == "ShowReceiptImages" {
            let itemsViewController = segue.destination as! ImageCollectionViewController
            itemsViewController.type = "receipt"
            itemsViewController.itemId = (item.value(forKey: "id") as! String?)!
        }
    }
    
    @IBAction func cancelToItemDetailViewController(_ segue:UIStoryboardSegue) {
    }
    
    @IBAction func saveAccountSelection(_ segue:UIStoryboardSegue) {
        let selectorViewController = segue.source as! AccountSelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            accountLabel.text = selectedItem
            accountid = selectorViewController.selectedItemId! //as NSString
        }
    }
    
    @IBAction func saveCategorySelection(_ segue:UIStoryboardSegue) {
        let selectorViewController = segue.source as! CategorySelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            categoryLabel.text = selectedItem
            categoryid = selectorViewController.selectedItemId! //as NSString
        }
    }
    
    @IBAction func saveManufacturerSelection(_ segue:UIStoryboardSegue) {
        let selectorViewController = segue.source as! ManufacturerSelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            manufacturerLabel.text = selectedItem
            manufacturerid = selectorViewController.selectedItemId! //as NSString
        }
    }
    
    /*
    @IBAction func saveImageSelection(_ segue:UIStoryboardSegue) {
        let imageViewController = segue.source as! ImageViewController
        if let image = imageViewController.imageView.image {
            if imageViewController.type == "receipt" {
                receiptImage = image
            } else {
                itemImage = image
            }
        }
    }*/
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker) {
        let dateformatter = DateFormatter()
        // MMMddyyyy is essentially the same as the "medium" format
        dateformatter.setLocalizedDateFormatFromTemplate("MMMddyyyyEEE")
//        dateformatter.dateStyle = DateFormatter.Style.short
        let dateString = dateformatter.string(from: sender.date)
        
        //        acquiredTextField.text = sender.date.weekDayName + " " + dateformatter.string(from: sender.date)
        acquiredTextField.text = "\(dateString)"
        acquired = sender.date
    }

    @objc func datePickerValueChanged2(_ sender: UIDatePicker) {
        let dateformatter = DateFormatter()
        // MMMddyyyy is essentially the same as the "medium" format
        dateformatter.setLocalizedDateFormatFromTemplate("MMMddyyyyEEE")
        //        dateformatter.dateStyle = DateFormatter.Style.short
        let dateString = dateformatter.string(from: sender.date)
        
        //        acquiredTextField.text = sender.date.weekDayName + " " + dateformatter.string(from: sender.date)
        retirementTextField.text = "\(dateString)"
        retirement = sender.date
    }
    
    @objc func datePickerValueChanged3(_ sender: UIDatePicker) {
        let dateformatter = DateFormatter()
        // MMMddyyyy is essentially the same as the "medium" format
        dateformatter.setLocalizedDateFormatFromTemplate("MMMddyyyyEEE")
        //        dateformatter.dateStyle = DateFormatter.Style.short
        let dateString = dateformatter.string(from: sender.date)
        
        //        acquiredTextField.text = sender.date.weekDayName + " " + dateformatter.string(from: sender.date)
        warrantyTextField.text = "\(dateString)"
        warranty = sender.date
    }
    
    @objc func deleteItemDelegate(_ sender: UIBarButtonItem)
    {
        let dialogMessage = UIAlertController(title: "Confirm", message: "Are you sure you want to delete this item?", preferredStyle: .alert)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            print("Ok button tapped")
            
            self.delegate.deleteItemDelegate(self)
            self.dismiss(animated: true, completion: nil)

        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
}
